<?php

namespace App\Repository;

use App\Entity\Lemma;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lemma|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lemma|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lemma[]    findAll()
 * @method Lemma[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LemmaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lemma::class);
    }

    public function findTopLemmas($lang)
    {
        /*
            Liste de tous les lemmes
        */

        // get du dernier backup
        $lemmas = $this->getBackupFile();
        // filtre pour les lemmes de premier niveau
        $lemmas = $this->filterTopLemmas($lemmas);

        $template = [
            "http://purl.org/dc/terms/identifier",
            "http://www.w3.org/2004/02/skos/core#prefLabel",
            "http://www.w3.org/2004/02/skos/core#editorialNote",
            "http://www.w3.org/2004/02/skos/core#definition",
            "http://www.w3.org/2004/02/skos/core#scopeNote",
            "http://www.w3.org/2004/02/skos/core#note",
            "http://www.w3.org/2004/02/skos/core#narrower",
            "http://www.w3.org/2004/02/skos/core#related",
            "http://purl.org/umu/uneskos#memberOf",
            "http://purl.org/dc/terms/created",
            "http://purl.org/dc/terms/modified"
        ];

        // preparation des lemmes selon le template et la langue
        $result = [];
        foreach ($lemmas as $lemma) {
            $lemma = $this->prepareLemma($template, $lemma, $lang);
            $result[] = $lemma;
        }
        // tri des lemmes par ordre alphabétique
        usort($result,
            function($a, $b) {
                return strcmp(
                    str_replace(array("É"), 'E', $a->{'prefLabel'}[0]),
                    str_replace(array("É"), 'E', $b->{'prefLabel'}[0])
                );
            }
        );

        return $result;
    }

    public function findLemmaById($id, $lang, $recursif=True)
    {   
        /*
            Objet lemma par id
        */

        // get du dernier backup
        $lemmas = $this->getBackupFile();
        // filtre pour obtenir un seul lemme
        $key = $this->searchKey(
            $lemmas,
            "http://purl.org/dc/terms/identifier",
            $id
        );
        if ($key === null) return null;

        $lemma_src = $lemmas[$key];

        $template = [
            "http://purl.org/dc/terms/identifier",
            "http://www.w3.org/2004/02/skos/core#prefLabel",
            "http://www.w3.org/2004/02/skos/core#editorialNote",
            "http://www.w3.org/2004/02/skos/core#definition",
            "http://www.w3.org/2004/02/skos/core#scopeNote",
            "http://www.w3.org/2004/02/skos/core#note",
            "http://www.w3.org/2004/02/skos/core#broader",
            "http://www.w3.org/2004/02/skos/core#narrower",
            "http://www.w3.org/2004/02/skos/core#related",
            "http://purl.org/umu/uneskos#memberOf",
            "http://purl.org/dc/terms/created",
            "http://purl.org/dc/terms/modified"
        ];
        
        // preparation du lemme selon le template et la langue
        $lemma = $this->prepareLemma($template, $lemma_src, $lang, $recursif);

        return $lemma;
    }

    public function getBackupFile($version=null)
    {
        /*
            Retourne l'obj json du dernier backup dans le dossier des backups
            Si $version n'est pas null, retourne l'obj json correspondant à cette version
        */
        // '%kernel.project_dir%/public/backup'
        $backup_dir = "backup";

        // si pas de version demandée
        if ($version === null) {
            $files = scandir($backup_dir, SCANDIR_SORT_DESCENDING);
            $newest_file = $files[0];
            $data = file_get_contents(sprintf("%s/%s", $backup_dir, $newest_file));
            return json_decode($data, true);  
        }

        // si version est demandée
        $version_filepath = sprintf("%s/%s.json", $backup_dir, $version);
        if (file_exists($version_filepath)) {
            // cas où la version existe bien dans le dossier backup, on retourne le .json correspondant
            $data = file_get_contents($version_filepath);
            return json_decode($data, true);
        } else {
            // si le fichier version n'existe pas, on retourne la dernière version
            return $this->getBackupFile();
        }
        
        
    }

    private function prepareLemma($template, $data, $lang, $recursif=True)
    {

        // pr récup les groupes : https://opentheso2.mom.fr/opentheso2/api/info/list?theso=th36&format=jsonld&group=all
        $groups = [
            "G1800" => "Concept",
            "G1804" => "Approche",
            "G1802" => "Méthode"
        ];
        $lemma = new Lemma();
        foreach ($template as $property) {
            $sep = (strpos($property, '#') == true) ? "#" : "/";
            $tmp = explode($sep, $property);
            $property_name = array_pop($tmp);
            $lemma->$property_name = [];
        }

        // boucle dans les propriétés du template
        foreach ($template as $property) {

            // si le lemme contient la propriété du template
            if (!isset($data[$property])) continue;

            $arr_values = $data[$property];

            // cas d'une $key qui ne serait pas un tableau (par ex. : "@id")
            if (!is_array($arr_values)) {
                $lemma->{$property} = $arr_values;
                continue;
            }

            // cas de la propriété identifiant
            if ($property == "http://purl.org/dc/terms/identifier") {
                $lemma->{"identifier"} = $arr_values[0]['@value'];
            
            } elseif ($property == "http://www.w3.org/2004/02/skos/core#prefLabel") {
                
                // recherche du prefLabel correspondant à $lang
                $first_label_key = $this->searchKey($arr_values, "@language", $lang);
                    
                // s'il y a bien un prefLabel lié à $lang
                if (!is_null($first_label_key)) {
                    // ajout du prefLabel lié à langue
                    $first_label = $arr_values[$first_label_key]["@value"];
                    $lemma->{"prefLabel"}[] = $first_label;

                    // recherche du "prefLabel" dans la langue opposée
                    $second_lang = ($lang == "fr") ? "en" : "fr";
                    $second_label_key = $this->searchKey($arr_values, "@language", $second_lang);
                    // s'il y a bien un prefLabel dans la langue opposée
                    if (!is_null($second_label_key)) {
                        $second_label = $arr_values[$second_label_key]["@value"];            
                        $lemma->{"prefLabel"}[] = $second_label;
                    }
                // s'il n'y pas de prefLabel pr $lang, on prend la première valeur
                } else {
                    $lemma->{"prefLabel"}[] = $arr_values[0]["@value"];
                }
            
            } else {

                // boucle dans le tableau des valeurs
                foreach ($arr_values as $value) {

                    // filtre de la valeur par la langue locale
                    if (!empty($value['@language']) && $lang === $value['@language']) {
                        $sep = (strpos($property, '#') == true) ? "#" : "/";
                        $tmp = explode($sep, $property);
                        $property_name = array_pop($tmp);
                        if ($property_name == "scopeNote") {
                            $dom = new \DomDocument();
                            // print_r($value['@value']);
                            $dom->loadHTML('<?xml encoding="utf-8" ?>'.$value['@value']);
                            $p = $dom->getElementsByTagName("p")[0];
                            $lemma->{$property_name}[] = array(
                                "header" => str_replace("&amp;", "&", html_entity_decode($dom->saveHTML($p))),
                                "value" => html_entity_decode($value['@value'])
                            );
                        } else {
                            // html_entity_decode($value['@value']);
                            $lemma->{$property_name}[] = html_entity_decode($value['@value']);
                        }
                    } else if (isset($value['@id'])) {
                        $sep = (strpos($property, '#') == true) ? "#" : "/";
                        $tmp = explode($sep, $property);
                        $property_name = array_pop($tmp);
                        $id_components = parse_url($value["@id"]);
                        parse_str($id_components['query'], $params);
                        
                        // $other_lemma = $this->findLemmaById($params["idc"], $lang);
                        if ($recursif) {
                            if ($property_name != "memberOf") {
                                $other_lemma = $this->findLemmaById($params["idc"], $lang, false);
                                $tmp = [];
                                $tmp["prefLabel"] = $other_lemma->{"prefLabel"}[0];
                                $tmp["identifier"] = $other_lemma->{"identifier"};
                                $lemma->{$property_name}[] = $tmp;
                            } else {
                                $lemma->{$property_name}[] = $groups[$params["idg"]];
                            }
                        }
                    // cas des dates
                    } else if (isset($value['@type'])) {
                        $sep = (strpos($property, '#') == true) ? "#" : "/";
                        $tmp = explode($sep, $property);
                        $property_name = array_pop($tmp);
                        $lemma->{$property_name} = html_entity_decode($value['@value']);
                    // cas des valeurs hors langue locale
                    } else {
                        $sep = (strpos($property, '#') == true) ? "#" : "/";
                        $tmp = explode($sep, $property);
                        $property_name = array_pop($tmp);
                        // $lemma->{$property_name}[] = "";
                    }
                }
            }

        }
        // var_dump($lemma);
        return $lemma;
    }

    private function searchKey($array, $property, $criteria)
    {
       foreach ($array as $key => $val) {
            
            // cas de "http://purl.org/dc/terms/identifier"
            if (is_array($val[$property])) {
                $val[$property] = $val[$property][0]['@value'];
            }
            if ($val[$property] === $criteria) {
                return $key;
            }
       }
       return null;
    }

    private function filterTopLemmas($list)
    {
        $isTopLemma = function ($var) {
            return !isset($var["http://www.w3.org/2004/02/skos/core#broader"]);
        };

        $list = array_filter($list, $isTopLemma);

        return $list;
    }

    // /**
    //  * @return Lemma[] Returns an array of Lemma objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lemma
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}

