document.addEventListener("DOMContentLoaded",  function() {
    initDetail();
}, false);

function initDetail() {

    // calcul de la valeur "top" en px pr les 1ers <p> des contributions
    calculateTopForStickyElements();

    // gestion de contributions/perspectives multiples
    var tabs_contrib = document.querySelectorAll(".container_tab_contrib .tab_contrib");
    tabs_contrib.forEach(tab => tab.addEventListener("click", handleMultipleContribClick));

}

function calculateTopForStickyElements() {
    /*
        - calcule la valeur "top" en px pr les 1ers <p> des contributions.
        - se base sur la hauteur du #h1_container.
        - ajoute en inline style la valeur "top" aux 1ers <p> des contributions.
    */
    var contribs = document.querySelectorAll("article.contribution");
    // attente du chargement des typos pour avoir la bonne hauteur
    document.fonts.ready.then(function () {
        var sticky_div = document.querySelector("#h1_container");
        var height = sticky_div.offsetHeight - 1;
        // a chaque 1er p de chaque contrib, on ajoute la valeur "top"
        contribs.forEach(contrib => contrib.querySelector("p").style.top = height+"px");
    });
}


function handleMultipleContribClick() {
    /*
        - get index du tab cliqué ds la collection des tabs
        - get de la cible contribution à faire apparaître
        - appelle toggleMultipleContribs() pour ajouter et reset .active
    */

    // get index du tab cliqué
    var tabs_contrib = document.querySelectorAll(".container_tab_contrib .tab_contrib");
    var tab = closest(this, function(el) { return el.classList.contains('tab_contrib'); });
    var index = [...tabs_contrib].indexOf(tab);
    
    // sélection du tab cliqué pour montrer la contribution correspondante
    var target = document.querySelectorAll("#container_multiple_contributions .contribution")[index];
    toggleMultipleContribs(tab, target);
}

function toggleMultipleContribs(tab, target) {
    /*
        - permet de montrer la contrib correspondant a un tab
        - ajoute et reset .active pour les tabs et les targets
    */

    var tab_already_active = document.querySelector(".container_tab_contrib .active");
    // ajout .active sur le tab voulu
    tab.classList.add("active");
    // reset des class .active autre que le tab voulu
    if (tab_already_active) tab_already_active.classList.remove("active");

    var target_already_active = document.querySelector("#container_multiple_contributions .active");
    // ajout .active sur la contribution voulue
    target.classList.add("active");
    // reset des class .active autre que la contribution voulue
    if (target_already_active) target_already_active.classList.remove("active");
}