


# Performascope


config/services.yaml

## Site bilingue

**Routes**

Chaque route doit avoir une locale spécifié : `@Route("/{_locale}"`. Le filtre langue des données d'Opentheso est réalisé dans LemmaRepository et Lemma.js.

L'interrupteur dans les templates (affiche la langue opposée à getLocale()) :

``` php
{% set locale_translation = app.request.getLocale() == "fr" ? 'en' : 'fr' %}
<a href="{{ path('list', {_locale: locale_translation}) }}">{{ locale_translation }}</a>
```

**Traduction**

Dans les controllers :

```
$locale = $request->getLocale();
```

`translations/messages.en.yaml` :

```
random_lemma: See a random entry
```

Traduction dans les templates :

```
{{ t('random_lemma')|trans }}
```


## Installation

- `git clone https://gitlab.com/performance-laboratory/glossaire-pl-symfony`
- `composer install`
- modifier le fichier `.env.local` :
    - "APP_ENV=env" => "APP_ENV=prod"
    - définir la database, par exemple : DATABASE_URL="mysql://symfony_user:symfony_userpassword@127.0.0.1:3306/symfony_dbname?serverVersion=5.7"
- `php bin/console doctrine:migrations:migrate`
- ajouter le dossier "fonts" dans `/public`
- `symfony server:start`
- `localhost:8000/`
- faire un backup à partir de `/admin/backup`




## Déploiement 

- `git clone https://gitlab.com/performance-laboratory/glossaire-pl-symfony`
- ou `git pull`
- modifier le fichier `.env.local` :
    - "APP_ENV=env" => "APP_ENV=prod"
    - définir la database, par exemple : DATABASE_URL="mysql://symfony_user:symfony_userpassword@127.0.0.1:3306/symfony_dbname?serverVersion=5.7"
- `composer install --no-dev --optimize-autoloader`
- `php bin/console doctrine:migrations:migrate`
- faire un backup à partir de `/admin/backup`
- ajouter le dossier "fonts" dans `/public`



## Mise à jour

- dans `composer.json` : changer les contraintes de version sur la clé "require" et "extra.symfony" de tous les packages débutant par "symfony/" 
- `composer update "symfony/*" --with-all-dependencies` pour mettre à jour y compris les dépendances
- `composer update` pour mettre à jour les autres packages
- `composer recipes` pour voir les mises à jours des recipes
- `composer recipes:install symfony/framework-bundle --force -v` pour mettre à jour une recipe spécifique



## Ressources

*Symfony :*

- versions : https://symfony.com/releases
- déploiement : https://symfony.com/doc/current/deployment.html
- déploiement apache : https://symfony.com/doc/current/setup/web_server_configuration.html
- mise à jour version mineure : https://symfony.com/doc/current/setup/upgrade_minor.html
- routes avec des préfixes de langues : https://symfony.com/doc/current/routing.html#localized-routes-i18n
- traduction : https://symfony.com/doc/current/translation.html
- multi-lingue :
    - https://github.com/doctrine-extensions/DoctrineExtensions/blob/main/doc/translatable.md
    - https://github.com/stof/StofDoctrineExtensionsBundle
    - https://github.com/doctrine-extensions/DoctrineExtensions/blob/main/doc/symfony4.md
- entity et repository : https://symfony.com/doc/current/doctrine.html#querying-for-objects-the-repository