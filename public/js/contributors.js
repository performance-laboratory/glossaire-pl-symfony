/* 
    Appelé par le template contributors.html.twig
*/

document.addEventListener("DOMContentLoaded",  function() {
	initContributors();
}, false);

/* TODO

faire un arr pr lister les premiers paragraphes des scopeNotes

[ 
    prefLabels [
        "",
        ""
    ],
    id: "",
    arr_scopeNote_first_p [
        "",
        ""
    ]
]

*/ 
async function initContributors() {
    // fetch des données json du backup opentheso
    var response = await fetch(data_url); // data_url declare dans base.html
    var json = await response.json();
    // preparation des objets Lemma
    var lemmas = json.map(el => Lemma(el, json));
    // console.log(lemmas);

    // pour chaque contributors, on cherche s'il est l'auteur d'une contribution
    var contributors = document.querySelectorAll(".contributor");
    var parser = new DOMParser();
    contributors.forEach(function (contributor, index) {
        var name = contributor.innerHTML;
        var results = lemmas.filter(function(lemma) {
            if (lemma.scopeNote.length) {
                var arr_first_p = lemma.scopeNote.map(function(scopeNote) {
                    var first_p = parser.parseFromString(scopeNote, 'text/html').querySelector("p");
                    if (first_p) return first_p.innerHTML;
                });
                return arr_first_p.join(" ").includes(name);
            }
        });
        results.forEach(function (lemma) {
            var clone = document.querySelector(".li_template").content.cloneNode(true);
            var li = clone.querySelector("li");
            var a = clone.querySelector("a");
            a.setAttribute("href", "detail/"+lemma.id);
            a.querySelectorAll("span")[0].innerHTML = lemma.prefLabel[0];
            a.querySelectorAll("span")[1].innerHTML = lemma.prefLabel[1];
            contributor.nextElementSibling.appendChild(li);
        });
    });
}
