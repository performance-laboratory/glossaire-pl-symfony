document.addEventListener("DOMContentLoaded",  function() {
	init();
}, false);

async function init() {

    var close_buttons = document.querySelectorAll(".close_button");
    close_buttons.forEach(el => el.addEventListener("click", handleCloseClick));
    
    // ajout event click sur les .toggler pr afficher le contenu en rapport
    var togglers = document.querySelectorAll(".toggler");
    togglers.forEach(el => el.addEventListener("click", handleTogglerClick));
}

function handleCloseClick() {
    var target = closest(this, function(el) { return el.classList.contains('close_target'); });
    closeTarget(target);
}

function closeTarget(el) {
    while (el.firstChild) el.removeChild(el.firstChild);
}

/* TOGGLER */

function handleTogglerClick() {
    executeTogglerClick(this);
}

function executeTogglerClick(el) {
    /*
        Affiche le contenu .toggler_target en fonction du .toggler choisi
    */

    // reset des .toggler (source) et des .toggler_target (cible)
    if (!el.classList.contains("toggler_target")) {
        var already_active = document.querySelectorAll(".toggler.active");
        if (already_active.length) already_active.forEach(el => el.classList.remove("active"));
        var already_active = document.querySelectorAll(".toggler_target.active");
        if (already_active.length) already_active.forEach(el => el.classList.remove("active"));
    }
    // on rend visible le .toggler_target qui correspond au .toggler
    var target = el.getAttribute("data-target");
    var toggler_targets = document.querySelectorAll(`.toggler_target[data-target="${target}"]`);
    toggler_targets.forEach(el => el.classList.toggle("active"));
    if (!el.classList.contains("toggler_target")) el.classList.add("active");

    // exception pour la page about : retour en haut de la page on changement de .toggler
    if (document.querySelector("#about_pages")) {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
}

function updateURL(searchParams) {
    if (searchParams === undefined) var searchParams = getURLparams();
    var url = getURL();
    if (history.pushState) {
        var newurl = url.origin + url.pathname + "?" + searchParams.toString();
        history.replaceState({path:newurl},'',newurl);
    }
}
function getURL() {
    var url = new URL(document.location.href);
    return url;
}
function getURLparams() {
    var url = self.getURL();
    var searchParams = new URLSearchParams(url.search);
    return searchParams;
}


/* MODULES */

// extrait les valeurs d'une colonne d'un array
var extractColumn = (arr, column) => arr.map(x=>x[column]);
// retire les "undefined" d'un array
var removeUndefined = (arr) => arr.filter(el=>el!==undefined);
var removeNull = (arr) => arr.filter(node => node.length);
function html_entity_decode(str) { return String(str).replace(/&nbsp;/g, ' ').replace(/&amp;nbsp;/g, ' '); }
var closest = function(el, fn) { return el && (fn(el) ? el : closest(el.parentNode, fn)); }

