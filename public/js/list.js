document.addEventListener("DOMContentLoaded",  function() {
	initList();
}, false);

function initList() {

    var id = 'list_container';
    if (document.getElementById(id)) var monTableau = new List(id);
}

class List {

    constructor() {
        this.data_fetched = [];
        this.data_prepared = [];
        this.data_updated = [];

        this.default_params = {
            "sort_by": "prefLabel",
            "sort_order": "asc"
        };

        this.list_container = document.querySelector("#list_container");
        this.list = d3.select("#list_container");
        this.initList();
    }

    async initList() {

        // fetch et prepare data
        await this.getData();

        // events

        // filter
        var cats = document.querySelectorAll("#filter_cats li");
        cats.forEach(el => el.addEventListener("click", e => this.submitFilter(e)));

        // sort_by
        var sort_by = document.querySelectorAll("#sort_by li");
        sort_by.forEach(el => el.addEventListener("click", e => this.submitSortby(e)));

        // sort_order
        var sort_orders = document.querySelectorAll("#sort_order li");
        sort_orders.forEach(el => el.addEventListener("click", e => this.submitSortorder(e)));

        // search
        var search_form = document.querySelector("form.searchbox ");
        search_form.addEventListener("submit", e => this.submitSearch(e));
        // rm search a l'init
        var searchParams = this.getURLparams();
        searchParams.delete("search");

        // UPDATE
        this.updateList(searchParams);
        this.updateCounter();
    }

    async getData() {
        await this.fetchData();
        this.prepareData();
    }

    async fetchData() {
        // data_url declare dans base.html
        var response = await fetch(data_url);
        var json = await response.json();
        this.data_fetched = json;
    }
    prepareData() {
        var data = this.data_fetched;
        this.data_prepared = data.map(d => new Lemma(d, this.data_fetched));
    }


    // UPDATES

    updateList(searchParams) {
        this.updateURL(searchParams);
        this.updateTools();
        this.updateData();
        this.updateRows();

        // console.log("data_fetched", this.data_fetched);
        // console.log("data_prepared", this.data_prepared);
        // console.log("data_updated", this.data_updated);
    }

    updateURL(searchParams) {
        if (searchParams === undefined) var searchParams = this.getURLparams();
        // si sort_by ou sort_order ne st pas définis, on prend la valeur par défaut
        if (!searchParams.has("sort_by")) searchParams.set("sort_by", this.default_params.sort_by);
        if (!searchParams.has("sort_order")) searchParams.set("sort_order", this.default_params.sort_order);

        var url = this.getURL();
        // on ajoute les paramètres à l'url sans recharger la page
        if (history.pushState) {
            var newurl = url.origin + url.pathname + "?" + searchParams.toString();
            history.replaceState({path:newurl},'',newurl);
        }
    }

    updateTools() {
        var searchParams = self.getURLparams();
        for (var pair of searchParams.entries()) {
            var select = document.querySelector(".tools .select[data-filter='"+pair[0]+"']");
            var option = pair[1];
            if (select) {
                // reset .selected
                var already_selected = select.querySelectorAll("li.selected");
                already_selected.forEach(el => el.classList.remove("selected"));

                var li = select.querySelector("li[data-option='"+option+"']");
                if (li) li.classList.add("selected");

                if (option == "∞") select.querySelectorAll("li").forEach(li=>li.classList.add("selected"));
            }
        }
    }
    
    updateData() {
        var data = this.data_prepared;
        var searchParams = this.getURLparams();

        // FILTER
        var filterParams = this.getURLfilters();
        var data = data.filter(function (d) {
            var boolean_array = [];
            filterParams.forEach(function(value, filter) {
                if (value == "∞") {
                    var bool = true;
                } else {
                    var result = d[filter].find(el => el.includes(value));
                    var bool = (result != undefined) ? true : false;
                }
                boolean_array.push(bool);
            })
            // on vérifie que ttes les bool soient true
            return boolean_array.every(el => el);
        });

        // SORT
        var sort_by = searchParams.get("sort_by");
        var sort_order = searchParams.get("sort_order");

        var collator = new Intl.Collator('fr', { numeric: true, sensitivity: 'base' });
        var data = data.sort((a, b) => collator.compare(a[sort_by], b[sort_by]));
        if (sort_order === "desc") data.reverse();

        // SEARCH
        if (searchParams.has("search")) {
            var query = searchParams.get("search").toLowerCase();
            var data = data.filter(function (d) {
                if (d.prefLabel.join(" ").toLowerCase().includes(query)) {
                    var bool = true;
                } else if (d.scopeNote.join(" ").toLowerCase().includes(query)) {
                    var bool = true;
                } else {
                    var bool = false;
                }
                return bool;
            });
        }

        this.data_updated = data;
    }

    updateRows() {
        var data = this.data_updated;

        var rows = this.list.selectAll(".term")
          .data(data, d => d.id)
          .join(
            enter => enter.append(() => document.querySelector(".term_template").content.cloneNode(true).querySelector("a")),
            update => update,
            exit => exit.remove()
          );
        rows.attr("href", function (d) {
            var href = this.getAttribute("href");
            if (href.includes(d.id)) { return href; }
            else { return href.slice(0, -1) + d.id; }
        });
        rows.select(".label_local")
            .text(d => d.prefLabel[0]);
        rows.select(".label_translated")
            .text(d => d.prefLabel[1]);
        rows.select(".notice")
            .attr("data-w", d => d.editorialNote.length);
        rows.select(".definition")
            .attr("data-w", d => d.definition.length);
        rows.select(".contribution")
            .attr("data-w", d => d.scopeNote.length);
        rows.select(".bibliography")
            .attr("data-w", d => d.note.length);
    }

    updateCounter() {
        var data = this.data_fetched;
        var lemmas_fr = data.map(el => Lemma(el, data, "fr"));
        var lemmas_en = data.map(el => Lemma(el, data, "en"));    
        var no_definition = lemmas_fr.reduce((total, lemma) => lemma.definition.length + total, 0);
        var no_lemmas_fr = lemmas_fr.reduce((total, lemma) => lemma.scopeNote.length + total, 0);
        var no_lemmas_en = lemmas_en.reduce((total, lemma) => lemma.scopeNote.length + total, 0);

        var clone = document.querySelector(".fiche_template").content.cloneNode(true);
        var fiche = clone.querySelector("div");

        var div_lemma = fiche.querySelector(".lemmas");
        div_lemma.querySelector("span").innerHTML = lemmas_fr.length;

        var div_contribution_fr = fiche.querySelectorAll(".corresp.contribution")[0];
        div_contribution_fr.querySelector("span").innerHTML = no_lemmas_fr;
        var div_contribution_en = fiche.querySelectorAll(".corresp.contribution")[1];
        div_contribution_en.querySelector("span").innerHTML = no_lemmas_en;

        var div_definition = fiche.querySelector(".corresp.definition");
        div_definition.querySelector("span").innerHTML = no_definition;

        var compteur_container = document.querySelector("#compteur_container");
        compteur_container.appendChild(fiche);
    }

    getURL() {
        var url = new URL(document.location.href);
        return url;
    }
    getURLparams() {
        var url = this.getURL();
        var searchParams = new URLSearchParams(url.search);
        return searchParams;
    }
    getURLfilters() {
        var searchParams = this.getURLparams();
        searchParams.delete("sort_by");
        searchParams.delete("sort_order");
        searchParams.delete("search");
        return searchParams;
    }

    submitSortby(e) {
        var el = e.currentTarget;
        var option = el.getAttribute("data-option");
        
        var searchParams = self.getURLparams();
        searchParams.set("sort_by", option);

        this.updateList(searchParams);
    }

    submitSortorder(e) {
        var el = e.currentTarget;
        var option = el.getAttribute("data-option");

        var searchParams = self.getURLparams();
        searchParams.set("sort_order", option);

        this.updateList(searchParams);
    }

    submitFilter(e) {
        var el = e.currentTarget;

        var select = closest(el, function(el) { return el.classList.contains('select'); });
        var filter = select.getAttribute("data-filter");
        var option = el.getAttribute("data-option");

        var options_selected = select.querySelectorAll("li.selected");
        if (el.classList.contains("selected") && options_selected.length == 1) option = "∞";
        
        var searchParams = this.getURLparams();
        if (option) {
            searchParams.set(filter, option);
        } else {
            searchParams.delete(filter);
        }

        this.updateList(searchParams);
    }

    submitSearch(e) {
        var el = e.currentTarget;
        var search_input = el.querySelector("input.search");
        var query = search_input.value;

        var searchParams = this.getURLparams();
        if (query.length) {
            searchParams.set("search", query);
        } else {
            searchParams.delete("search");
        }

        this.updateList(searchParams);
    }
}






