var template = [
    "http://www.w3.org/2004/02/skos/core#prefLabel",
    "http://www.w3.org/2004/02/skos/core#editorialNote",
    "http://www.w3.org/2004/02/skos/core#definition",
    "http://www.w3.org/2004/02/skos/core#scopeNote",
    "http://www.w3.org/2004/02/skos/core#note",
    "http://www.w3.org/2004/02/skos/core#broader",
    "http://www.w3.org/2004/02/skos/core#narrower",
    "http://www.w3.org/2004/02/skos/core#related",
    "http://purl.org/umu/uneskos#memberOf",
    "http://purl.org/dc/terms/created",
    "http://purl.org/dc/terms/modified"
];

var collections = {
    "G1222": "Méthode",
    "G1224": "Concept",
    "G1226": "Approche",
    "G1800": "Concept",
    "G1804": "Approche",
    "G1802": "Méthode"
};

function Lemma(data, data_fetched, lang) {
    if (lang === undefined) var lang = user_lang; // "lang" dans base.html

    var lemma = {};
    lemma.id = data["http://purl.org/dc/terms/identifier"][0]["@value"];
    for (var i = 0; i < template.length; i++) {
        var property_fullname = template[i];
        var sep = (property_fullname.includes('#') == true) ? "#" : "/";
        var tmp = property_fullname.split(sep);
        var property_name = tmp[tmp.length - 1];
        lemma[property_name] = [];
    }

    for (var i = 0; i < template.length; i++) {
        var property_fullname = template[i];

        // si le lemme contient la propriété du template
        if (data.hasOwnProperty(property_fullname)) {
            var arr_values = data[property_fullname];
        } else {
            var arr_values = [];
        }
        var sep = (property_fullname.includes('#') == true) ? "#" : "/";
        var tmp = property_fullname.split(sep);
        var property_name = tmp[tmp.length - 1];

        // cas de la propriété identifiant
        if (property_name == "prefLabel") {
            lemma["prefLabel"] = prepareLabels(arr_values);
        } else {
            // boucle dans le tableau des valeurs
            for (var j = 0; j < arr_values.length; j++) {
                var value = arr_values[j];
                // filtre de la valeur par la langue locale
                if (value.hasOwnProperty('@language') && lang == value['@language']) {
                    lemma[property_name].push(value['@value']);
                } else if (value.hasOwnProperty('@id')) {
                    if (property_name == "memberOf") {
                        var coll_id = new URL(value["@id"]).searchParams.get("idg");
                        lemma[property_name].push(collections[coll_id]);
                    } else {
                        var other_lemma_id = new URL(value["@id"]).searchParams.get("idc");
                        var other_lemma = data_fetched.find(el => el["@id"].includes(other_lemma_id));
                        var other_lemma_labels = prepareLabels(other_lemma["http://www.w3.org/2004/02/skos/core#prefLabel"]);
                        lemma[property_name].push({
                            "id": other_lemma_id,
                            "prefLabel": other_lemma_labels
                        });
                    }
                // cas des dates
                } else if (value.hasOwnProperty('@type')) {
                    lemma[property_name] = value['@value'];
                }
            }
        }
    }
    return lemma;
}

/*
    Prepare un tableau de labels
    [0] est le label dans la langue du site
    [1] est le label dans l'autre langue
*/
function prepareLabels(arr_values) {
	var lang = user_lang;
    var labels_array = [];
    // recherche du prefLabel correspondant à lang
    var correct_label = arr_values.find(el => el['@language'] == lang);
    if (correct_label != undefined) {
        // ajout du prefLabel lié à langue
        labels_array.push(correct_label["@value"]);
        // recherche du "prefLabel" dans la langue opposée
        var second_lang = (lang == "fr") ? "en" : "fr";
        var second_label = arr_values.find(el => el['@language'] == second_lang);
        // s'il y a bien un prefLabel dans la langue opposée
        if (second_label != undefined) {
            labels_array.push(second_label["@value"]);
        }
    // s'il n'y pas de prefLabel pr lang, on prend la première valeur
    } else {
        labels_array.push(arr_values[0]["@value"]);
    }
    return labels_array;
}