# Performascope

## Résumé

Le site Performascope est basé sur Symfony. Les données des pages
statiques (présentation, page "A propos", liste des contributeurs) sont
administrées par Symfony. Les données du lexique sont administrées par
Opentheso, une copie en est faite dans le dossier public de `Symfony`.

## Liens

-   Le site :
    [performance-lab.huma-num.fr/](../../../Téléchargements/performance-lab.huma-num.fr/)

-   L'admin Symfony :
    [performance-lab.huma-num.fr/admin](../../../Téléchargements/performance-lab.huma-num.fr/admin)

-   URL du backup : <https://performance-lab.huma-num.fr/fr/backup>

-   L'instance Opentheso d'Huma-Num : <https://opentheso.huma-num.fr/>

-   L'API Opentheso :
    [api](https://opentheso.huma-num.fr/opentheso/api/search?q=action&theso=th274&format=jsonld)

-   git : https://gitlab.com/performance-laboratory/glossaire-pl-symfony

## Base de données

-   la base de données est : `_pl_glossaire_symfony`

## Symfony

### Se connecter à l'administration

-   se rendre sur cette url :
    [performance-lab.huma-num.fr/admin](../../../Téléchargements/performance-lab.huma-num.fr/admin)

-   rentrer les identifiants

### Ajouter une page statique

-   cliquer sur "Pages" dans la colonne de gauche

-   cliquer sur le bouton bleu "Add Page" en haut à droite de la page

-   requis : renseigner un `slug` (une portion d'url unique dans le
    site, sans caractères spéciaux, pour retrouver cette page)

-   requis : renseigner un "title" en français et en anglais

-   renseigner un "body" en français et en anglais (c'est le texte long
    de la page)

-   cliquer sur le bouton bleu "Create" en haut à droite

https://www.markdownguide.org/basic-syntax/

### Éditer une page statique

-   cliquer sur "Pages" dans la colonne de gauche

-   cliquer sur les trois points au bout à droite des lignes des pages

-   cliquer sur "Edit" pour éditer la page

-   cliquer sur le bouton bleu "Save changes" en haut à droite

### Supprimer une page statique

-   cliquer sur "Pages" dans la colonne de gauche

-   cliquer sur les trois points au bout à droite des lignes des pages

-   cliquer sur "Delete" pour éditer la page

-   confirmer la suppression

### Ajouter une page dans la page "A propos"

-   cliquer sur "Pages" dans la colonne de gauche

-   cliquer sur une page à éditer

-   dans la partie "About order", renseigner un chiffre qui déterminera
    le rang d'apparition de cette page dans "A propos"

-   si rien n'est renseigné, cette page n'apparaît pas dans "A propos"

### Pages spéciales

-   le contenu de la page au slug "introduction" est utilisé sur la page
    d'accueil

## Faire un backup

-   se rendre sur cette page :
    <https://performance-lab.huma-num.fr/fr/backup>

-   une requête est alors lancée vers l'API d'Opentheso, un fichier est
    alors stocké dans `public/backup`

## Opentheso

Se connecter à l'administration

-   se rendre sur cette url : <https://opentheso.huma-num.fr/opentheso/>

-   cliquer sur le symbole d'une clé en haut à gauche

-   rentrer les identifiants

-   dans l'input select "Choisir un thésaurus", choisir le thésaurus du
    Performascope

### Ajouter un terme

-   <https://opentheso.hypotheses.org/category/creer-un-thesaurus#ajouter-top-concept>

### Renommer ou supprimer un terme

-   <https://opentheso.hypotheses.org/category/creer-un-thesaurus#Modifier-concept>

### Associer deux termes

-   <https://opentheso.hypotheses.org/category/creer-un-thesaurus#Relationsassociatives>

### Ajouter ou supprimer une catégorie à un terme

-   sur la page du terme, au niveau de "Collection" cliquer le symbole
    de rouage

-   choisir "Ajouter à un groupe" ou "Supprimer d'un groupe"

-   si "Ajouter à un groupe" : choisir "Approche", "Concept" ou
    "Méthode"

-   si "Supprimer d'un groupe" : cliquer sur le symbole poubelle en face
    de la catégorie à supprimer

### Ajouter une traduction à un terme

-   <https://opentheso.hypotheses.org/category/creer-un-thesaurus#Traductions>

-   pour ajouter du contenu dans une autre langue, cliquer sur le nom du
    terme traduit

### Ajouter une note (une "définition", une "perspective", une "citation",
une "bibliographie")

#### Préparer le contenu html

Il faut que le contenu de la note soit en html mais Opentheso ne permet
actuellement pas l'édition en html dans son éditeur de texte. Il faut
alors générer le html soit même ou alors passer par un outil comme
CKEditor

-   avec CKEditor, se rendre sur cette page
    <https://ckeditor.com/docs/ckeditor4/latest/examples/sourcearea.html#cke_editor1>

-   supprimer le contenu déjà présent et insérer son propre contenu

-   cliquer sur le bouton "Source" et copier le contenu

-   veiller à ce qu'il n'y ait pas de lignes vides entre les balises
    `<p>`

-   pour une "perspective" :

    -   il faut impérativement que le premier paragraphe soit sous cette
        forme : "\[Prénom\] \[Nom\], \[Fonction\], \[Champ disciplinaire
        ou Domaine scientifique\], \[Laboratoire\], \[Université ou
        autre institution de rattachement\]"

    -   il faut impérativement que le dernier paragraphe soit sous cette
        forme : "Pour citer : ..."

-   pour une "citation" :

    -   il faut impérativement que le dernier paragraphe soit sous cette
        forme : "Pour citer : ..."

-   pour une "bibliographie", il faut simplement faire une liste de
    paragraphes et non de multiples entrées comme pour les autres types
    de notes

#### Ajouter le contenu dans Opentheso

-   <https://opentheso.hypotheses.org/category/creer-un-thesaurus#Annotations>

-   dans le premier input select, choisir :

    -   "Note éditoriale (Editorial note)" pour ajouter une "définition"

    -   "Note d'aplication (Scope note)" pour ajouter une "perspective"

    -   "Définition (Definition)" pour ajouter une "citation"

    -   "Note (Note)" pour ajouter une "bibliographie"

-   dans le champ de texte "ajouter une valeur", coller le contenu html

-   cliquer sur "Valider"

## Afficher les derniers ajouts sur le site du Performascope

-   faire un backup comme indiqué dans la partie Symfony \> Faire un
    backup
