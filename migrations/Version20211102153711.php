<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211102153711 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE about DROP FOREIGN KEY FK_B5F422E3401ADD27');
        $this->addSql('DROP INDEX IDX_B5F422E3401ADD27 ON about');
        $this->addSql('ALTER TABLE about DROP pages_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE about ADD pages_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE about ADD CONSTRAINT FK_B5F422E3401ADD27 FOREIGN KEY (pages_id) REFERENCES conference (id)');
        $this->addSql('CREATE INDEX IDX_B5F422E3401ADD27 ON about (pages_id)');
    }
}
