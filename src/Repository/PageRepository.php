<?php

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    /*
        Retourne les pages selon leur ordre aboutOrder
    */
    public function findAboutPages($locale)
    {
        $bd_pages = $this->createQueryBuilder('p')
            ->andWhere('p.aboutOrder IS NOT NULL')
            ->orderBy('p.aboutOrder', 'ASC')
            ->getQuery()
            ->getResult()
        ;
        
        $pages = [];
        for($i = 0; $i < count($bd_pages); ++$i) {
            $this->translate($bd_pages[$i], $locale);
            $pages[] = $bd_pages[$i];
        }
        return $pages;
    }

    /*
        Retourne l'objet page en filtrant selon la langue
    */
    public function translate($obj, $locale)
    {   
        if ($locale == "fr") {
            $obj->title = $obj->getTitleFr();
            $obj->body = $obj->getBodyFr();
        } else {
            $obj->title = $obj->getTitleEn();
            $obj->body = $obj->getBodyEn();
        }
        return $obj;
    }
    
    // /**
    //  * @return Page[] Returns an array of Page objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Page
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
