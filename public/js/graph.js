document.addEventListener("DOMContentLoaded",  function() {
	initGraph();
}, false);

async function initGraph() {

    var svg_container = document.querySelector(".svg_container");
    if (svg_container) {
        // data_url declare dans base.html
        var response = await fetch(data_url);
        json = await response.json();
        // console.log(json);
        graph = generateGraphData(json);
        drawGraph(json, graph);
    }

}

function generateGraphData(data) {
    var collections = {
        "G1222": "Méthode",
        "G1224": "Concept",
        "G1226": "Approche",
        "G1800": "Concept",
        "G1804": "Approche",
        "G1802": "Méthode"
    };

    // clone du tableau json pour créer l'obj "nodes"
    var nodes_src = JSON.parse(JSON.stringify(data));
    // construction du tableau des nodes
    var nodes = nodes_src.map(function (node, i) {
        var template = [
            "http://www.w3.org/2004/02/skos/core#prefLabel",
            "http://www.w3.org/2004/02/skos/core#editorialNote",
            "http://www.w3.org/2004/02/skos/core#definition",
            "http://www.w3.org/2004/02/skos/core#scopeNote",
            "http://www.w3.org/2004/02/skos/core#note",
            "http://www.w3.org/2004/02/skos/core#broader",
            "http://www.w3.org/2004/02/skos/core#narrower",
            "http://www.w3.org/2004/02/skos/core#related",
            "http://purl.org/umu/uneskos#memberOf",
            "http://purl.org/dc/terms/created",
            "http://purl.org/dc/terms/modified"
        ];
        var lemma = new Lemma(node, nodes_src);
        // console.log(lemma);
        lemma.id = node["http://purl.org/dc/terms/identifier"][0]["@value"];
        return lemma;
    });

    // clone du tableau json pour créer l'obj "links"
    var links_src = JSON.parse(JSON.stringify(data));
    var links = [];
    for (var i = 0; i < links_src.length; i++) {
        var node = links_src[i];
        /*
        var narrowers = node["http://www.w3.org/2004/02/skos/core#narrower"];
        if (narrowers) {
            var node_links = generateLinkData(node, "http://www.w3.org/2004/02/skos/core#narrower");
            links.push(node_links);
        }
        */
        var related = node["http://www.w3.org/2004/02/skos/core#related"];
        if (related) {
            var node_links = generateLinkData(node, "http://www.w3.org/2004/02/skos/core#related");
            links.push(node_links);
        }
    }
    // création obj "graph" pour d3
    var graph = {
        "nodes": nodes,
        "links": links.flat()
    }

    // console.log("GRAPH", graph);
    return graph;
}

function generateLinkData(node, name) {
    var arr = [];
    var val = node[name];
    
    for (var i = 0; i < val.length; i++) {
        var url = new URL(val[i]["@id"]);
        var target_id = url.searchParams.get('idc');
        var obj = {
            "source": node["http://purl.org/dc/terms/identifier"][0]["@value"],
            "target": target_id,
            "nature": name
        }
        arr.push(obj);
    }
    return arr.flat();
}

function drawGraph(data_src, data) {

    // VARIABLES
    var color = d3.scaleOrdinal(d3.schemeCategory10);
    var graph = data;
    var svg_container = document.querySelector(".svg_container");
    var svg_boundings = svg_container.getBoundingClientRect();
    var width = svg_boundings.width;
    var height = svg_boundings.height;
    
    var svg = d3.select(".svg_container").append("svg")
        .attr("viewBox", [0, 0, width, height]);

    const simulation = d3.forceSimulation()
            .force("charge", d3.forceManyBody().strength(-400))
            .force("link", d3.forceLink().id(d => d.id).distance(60))
            .force("x", d3.forceX())
            .force("y", d3.forceY())
    
    simulation.on("tick", () => {
        link
            .attr("x1", d => d.source.x)
            .attr("y1", d => d.source.y)
            .attr("x2", d => d.target.x)
            .attr("y2", d => d.target.y);

        node
            .attr("cx", d => d.x)
            .attr("cy", d => d.y);

        node_label
            .attr("transform", d => "translate(" + (d.x + 8) + "," + (d.y + 3) + ")");

    });


    // ZOOM
    var g = svg.append("g")
        .attr("class", "container");

    zoom = d3.zoom()
        .scaleExtent([.5, 4])
        .extent([[0, 0], [width, height]])
        .on('zoom', handleZoom);
    
    svg.call(zoom)
        .on("wheel.zoom", null);
    
    zoomReset();

    let link = g.append("g")
            .attr("stroke", "#000")
            .attr("stroke-width", 1)
          .selectAll("line");

    let node = g.append("g")
        .selectAll("circle")
        .call(drag(simulation));

    let node_label = g.append("g")
        .attr("class", "node_labels")
        .selectAll("text")
        .call(drag(simulation));


    function draw(graph) {
        var nodes = graph.nodes;
        var links = graph.links;

        // Make a shallow copy to protect against mutation, while
        // recycling old nodes to preserve position and velocity.
        const old = new Map(node.data().map(d => [d.id, d]));
        nodes = nodes.map(d => Object.assign(old.get(d.id) || {}, d));
        links = links.map(d => Object.assign({}, d));

        node = node
            .data(nodes, d => d.id)
            .join(enter => enter.append("circle")
                .attr("r", 2)
                .attr("fill", d => color(d.type)))
            .call(drag(simulation))
            .on("click", function() {
                handleNodeClick(data, d3.select(this).datum().id);
            });

        node_label = node_label
          .data(nodes, d => d.id)
          .join(enter => enter.append("text")
                .text(d => d.prefLabel[0])
                .style("font-size", "8px")
                .attr('x', 0)
                .attr('dy', '5'))
          .call(drag(simulation))
            .on("click", function() {
                handleNodeClick(data, d3.select(this).datum().id);
            });

        link = link
            .data(links, d => [d.source, d.target])
            .join("line")
            .attr("stroke-width", .2)
            .attr("stroke", function(d) {
                if (d.nature == "relation") {
                    return "lightgrey";
                } else {
                    return "grey";
                }
            });

        simulation.nodes(nodes);
        simulation.force("link").links(links);
        simulation.alpha(1).restart();

        // center si param url
        var searchParams = getURLparams();
        var fiche_container = document.querySelector("#fiche_container");
        if (searchParams.has("id") && !fiche_container.hasChildNodes()) {
            setTimeout(() => {
                var intro = document.querySelector("#introduction_container");
                if (intro) closeTarget(intro);
                displayFiche(data, searchParams.get("id"));
            }, 1000);
        }
    }

    // AJOUT DES EVENTS
    var li_collections = document.querySelectorAll("#select_cats li");
    li_collections.forEach(li => li.addEventListener('click', change));

    function change() {
        // console.log(data_src);

        var select_cats = document.querySelector("#select_cats");
        var options_selected = select_cats.querySelectorAll("#select_cats li.selected");
        // reset .selected
        var already_selected = select_cats.querySelectorAll("li.selected");
        already_selected.forEach(el => el.classList.remove("selected"));

        var option = this.getAttribute("data-option");
        this.classList.add("selected");

        if (this.classList.contains("selected") && options_selected.length == 1) {
            option = "∞";
            select_cats.querySelectorAll("li").forEach(li=>li.classList.add("selected"));
        }
        
        var nodes_filtered = data.nodes;
        var links_filtered = data.links;
        
        if (option != "∞") {
        var nodes_filtered = nodes_filtered.filter(function(el) {
            if (el.memberOf.includes(option)) {
                return el;
            }
        });
        }

        var id_list = extractColumn(nodes_filtered, "id");
        var links_filtered = links_filtered.filter(function(link) {
            return id_list.includes(link.source) && id_list.includes(link.target)
        });

        var temp_graph = {
            nodes: nodes_filtered,
            links: links_filtered
        };

        if (option) {
            var res = temp_graph;
        } else {
            var res = data;
        }
        // update du graph
        draw(res);
        
    }

    // init du graph
    draw(graph);
}

// DRAG DES CIRCLES
var drag = function (simulation) {
    function dragstarted(event, d) {
        if (!event.active) simulation.alphaTarget(0.3).restart()
        d.fx = d.x;
        d.fy = d.y;
    }

    function dragged(event, d) {
        d.fx = event.x;
        d.fy = event.y;
    }

    function dragended(event, d) {
        if (!event.active) simulation.alphaTarget(0);
        d.fx = undefined;
        d.fy = undefined;
    }
    
    return d3.drag()
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended);
}

// ZOOM
function handleZoom(e) {
    d3.select('svg g')
        .attr('transform', e.transform);
}
function zoomIn() {
    d3.select('svg')
        .transition()
        .call(zoom.scaleBy, 1.5);
}
function zoomOut() {
    d3.select('svg')
        .transition()
        .call(zoom.scaleBy, 0.5);
}
function zoomReset() {
    d3.select('svg')
        .call(zoom.translateTo, 0, 0)
        .call(zoom.scaleTo, 1);
}
function zoomCenter(x,y) {
    // var current_scale = d3.zoomTransform(d3.select('svg').node()).k;
    d3.select('svg')
        .transition()
        .call(zoom.translateTo, x, y);
}

function resetGraph() {
    // reset .selected et .not_active
    var already_selected = document.querySelectorAll("svg .selected");
    if (already_selected) already_selected.forEach(el => el.classList.remove("selected"));
    var already_not_active = document.querySelectorAll("svg .not_active");
    if (already_not_active) already_not_active.forEach(el => el.classList.remove("not_active"));

    // reset url
    var searchParams = getURLparams();
    searchParams.delete('id');
    updateURL(searchParams);

    // close fiche
    var fiche_container = document.querySelector("#fiche_container");
    closeTarget(fiche_container);

    zoomReset();
}

function handleNodeClick(data, id) {
    var fiche_container = document.querySelector("#fiche_container");
    while (fiche_container.firstChild) fiche_container.removeChild(fiche_container.firstChild);
    
    var intro = document.querySelector("#introduction_container");
    if (intro) closeTarget(intro);

    // reset .selected et .not_active
    var already_selected = document.querySelectorAll("svg .selected");
    if (already_selected) already_selected.forEach(el => el.classList.remove("selected"));
    var already_not_active = document.querySelectorAll("svg .not_active");
    if (already_not_active) already_not_active.forEach(el => el.classList.remove("not_active"));


    displayFiche(data, id);
}

function displayFiche(data, id) {
    if (id == undefined) {
        var random = Math.floor(Math.random() * (data.nodes.length - 0) + 0);
        var datum = data.nodes[random];
        var id = data.nodes[random].id;
    } else {
        var datum = data.nodes.find(el => el.id.includes(id));
    }
    var svg = d3.select("svg");
    var g = d3.select("svg > g");

    var labels = d3.selectAll(".node_labels text");
    var lines = d3.selectAll("svg line");

    var ids_related = [];
    ids_related.push(id);
    for (var i = 0; i < data.links.length; i++) {
        var el = data.links[i];
        if (el.source === id || el.target === id) {
            ids_related.push(el.source);
        }
    }

    var label_target = labels
        .filter(node => node.id == datum.id)
        .classed('selected', true);

    var labels_not_targets = labels
        .filter(node => !ids_related.includes(node.id))
        .attr('class', "not_active");

    var links_not_targets = lines
        .filter(line => !(line.source.id === id || line.target.id === id))
        .attr('class', "not_active");

    // console.log(label_target);
    var cx = label_target.data()[0].x;
    var cy = label_target.data()[0].y;
    
    zoomCenter(cx, cy);

    var searchParams = getURLparams();
    searchParams.set("id", id);
    updateURL(searchParams);

    var lang = user_lang;
    var clone = document.querySelector(".fiche_template").content.cloneNode(true);
    var fiche = clone.querySelector("div");
    fiche.querySelector("a").setAttribute("href", lang+"/detail/"+id);
    fiche.querySelector(".title").querySelector("span").innerHTML = datum.prefLabel[0];
    if (datum.editorialNote.length > 0) {
        fiche.querySelector("article.notice").innerHTML = html_entity_decode(datum.editorialNote[0]);
    } else {
        var container = closest(fiche.querySelector("article.notice"), function(el) { return el.classList.contains('row'); });
        container.parentNode.removeChild(container);
    }
    for (var i = 0; i < datum.related.length; i++) {
        var li = document.createElement("LI");
        li.setAttribute("data-id", datum.related[i]["id"]);
        li.innerHTML = datum.related[i]["prefLabel"][0];
        fiche.querySelector(".related").appendChild(li);
        li.addEventListener("click", function() {
            handleNodeClick(data, this.getAttribute("data-id"));
        });
    }
    fiche.querySelector(".close_button").addEventListener("click", handleCloseClick);
    fiche.querySelector(".close_button").addEventListener("click", resetGraph);
    // fiche.querySelector(".close_button").addEventListener("click", zoomReset);
    fiche_container.appendChild(fiche);
}
