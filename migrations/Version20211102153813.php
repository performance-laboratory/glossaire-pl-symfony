<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211102153813 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conference ADD in_about_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE conference ADD CONSTRAINT FK_911533C8977EA705 FOREIGN KEY (in_about_id) REFERENCES about (id)');
        $this->addSql('CREATE INDEX IDX_911533C8977EA705 ON conference (in_about_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE conference DROP FOREIGN KEY FK_911533C8977EA705');
        $this->addSql('DROP INDEX IDX_911533C8977EA705 ON conference');
        $this->addSql('ALTER TABLE conference DROP in_about_id');
    }
}
