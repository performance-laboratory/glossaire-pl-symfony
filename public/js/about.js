/* 
    Appelé par le template about.html.twig
*/

document.addEventListener("DOMContentLoaded",  function() {
    initAbout();
}, false);

function initAbout() {
    // s'il y a une ancre dans l'url on affiche 

    var anchor = window.location.hash.substr(1); // get du lien ancre
    if (anchor) {
        // get du lien .toggler lié à l'ancre de l'url
        var toggler = document.querySelector(`.toggler[data-target="${anchor}"]`);
        // fonction pr faire apparaître le contenu lié au 
        executeTogglerClick(toggler);
    }
}