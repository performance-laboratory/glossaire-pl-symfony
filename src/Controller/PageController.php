<?php

namespace App\Controller;

use App\Entity\Contributor;
use App\Repository\ContributorRepository;
use App\Entity\Page;
use App\Repository\PageRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="page_", requirements={"_locale": "fr|en"})
 */
class PageController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexNoLocale(): Response
    {
        return $this->redirectToRoute('page_graph', ['_locale' => 'fr']);
    }
    
    /**
     * @Route("/{_locale}", name="graph", requirements={"_locale": "fr|en"})
     */
    public function graph(Request $request, PageRepository $pageRepository): Response
    {
        # route graph + home

        $locale = $request->getLocale();

        # si url contient ?id= ou ?layout=graph alors $is_graph=true
        # pour ne pas afficer le block d'introduction
        $param_id = $request->query->get('id');
        $param_layout = $request->query->get('layout');
        $is_graph = ($param_layout || $param_id) ? true : false;

        # get de la page "introduction"
        $page_introduction = $pageRepository->findBy(['slug' => 'introduction']);
        $field_name = ($locale == "fr") ? "getBodyFr" : "getBodyEn";
        $introduction = (count($page_introduction)) ? $page_introduction[0]->{$field_name}() : "";

        return $this->render('lemma/graph.html.twig', [
            'is_graph' => $is_graph,
            'introduction' => $introduction
        ]);
    }


    /**
     * @Route("/{_locale}/page/{slug}", name="detail")
     */
    public function detail(Page $page, Request $request, PageRepository $pageRepository): Response
    {
        # route detail des pages statiques

        $locale = $request->getLocale();
        $pageRepository->translate($page, $locale);
        
        return $this->render('page/detail.html.twig', [
            'page' => $page
        ]);
    }

    /**
     * @Route("/{_locale}/about", name="about")
     */
    public function about(Request $request, PageRepository $pageRepository): Response
    {   
        # route pour la page about (collection de pages statiques)

        $locale = $request->getLocale();
        return $this->render('page/about.html.twig', [
            'pages' => $pageRepository->findAboutPages($locale),
        ]);
    }

    /**
     * @Route("/{_locale}/contributors", name="contributors")
     */
    public function contributors(Request $request, ContributorRepository $contributorRepository): Response
    {   
        # route pour la liste des contributeurs

        $contributors = $contributorRepository->findAll();
        return $this->render('page/contributors.html.twig', [
            'contributors' => $contributors,
        ]);
    }
}
