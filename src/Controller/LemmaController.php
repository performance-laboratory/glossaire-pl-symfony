<?php

namespace App\Controller;

use App\Entity\Lemma;
use App\Repository\LemmaRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

class LemmaController extends AbstractController
{

    /**
     * @Route("/{_locale}/list", name="list")
     */
    public function list(Request $request, LemmaRepository $lemmaRepository): Response
    {
        // route liste de tous les lemmes
        
        // $locale = $request->getLocale();
        // $lemmas = $lemmaRepository->findTopLemmas($locale);

        return $this->render('lemma/list.html.twig', []);
    }
    /**
     * @Route(
     *  "/{_locale}/detail/{id}",
     *  name="detail",
     *  defaults={"id": null}
     * )
     */
    public function detail($id, Request $request, LemmaRepository $lemmaRepository): Response
    {  
        // route detail d'un lemme
     
        $locale = $request->getLocale();
        $lemma = $lemmaRepository->findLemmaById($id, $locale);
        if ($lemma === null) throw $this->createNotFoundException();

        return $this->render('lemma/detail.html.twig', [
            'term' => $lemma
        ]);
    }

    /**
     * @Route(
     *  "/{_locale}/print",
     *  name="list_print"
     * )
     */
    /*
    public function list_print(Request $request, LemmaRepository $lemmaRepository): Response
    {
        // route liste de tous les lemmes pour le print

        $locale = $request->getLocale();
        $lemmas = $lemmaRepository->findTopLemmas($locale);
        return $this->render('lemma/list_print.html.twig', [
            'terms' => $lemmas
        ]);
    }
    */

    /**
     * @Route(
     *  "/{_locale}/print/{id}",
     *  name="detail_print",
     *  defaults={"id": null}
     * )
     */
    public function detail_print($id, Request $request, LemmaRepository $lemmaRepository): Response
    {   
        // route detail d'un lemme pour le print

        $locale = $request->getLocale();
        $lemma = $lemmaRepository->findLemmaById($id, $locale);
        if ($lemma === null) throw $this->createNotFoundException();
        return $this->render('lemma/detail_print.html.twig', [
            'term' => $lemma
        ]);
    }

    /**
     * @Route("/{_locale}/backup", name="backup")
     */
    public function backup(Request $request): Response
    {
        // route pour faire un backup a partir d'opentheso

        // requete a l'API
        $api_url = "https://opentheso.huma-num.fr/opentheso/api/all/theso?id=th274&format=jsonld";
        $conceptType = "http://www.w3.org/2004/02/skos/core#Concept";
        // $api_response = $this->sendCURL($api_url);
        ini_set("allow_url_fopen", 1);
        $api_response = file_get_contents($api_url);

        // gestion de la réponse
        if ($api_response) {
            // try to keep concepts only
            $concepts = [];
            $jsonElements = json_decode($api_response, 1);
            foreach ($jsonElements as $jsonElement) {
                if($jsonElement["@type"][0] == $conceptType) {
                    $concepts[] = $jsonElement;
                }
            }
            $concepts = json_encode($concepts);

            // création du nom et chemin du fichier selon la date du jour
            $date = (new \DateTime())->format('Y_m_d_H_i_s');
            $backup_dir = 'backup';
            $file_path = sprintf("%s/%s.json", $backup_dir, $date);
            // var_dump($file_path);
            // écriture du fichier
            $filesystem = new Filesystem();
            $filesystem->dumpFile($file_path, $concepts);

            $message = "Le backup a été fait.";
        } else {
            // erreur réponse api
            $message = "Problème avec l'api, le backup n'a pas pu être fait.";
        }

        return $this->render('lemma/backup.html.twig', [
            'message' => $message
        ]);
    }
    /*
    private function sendCURL($url) {
        $ch = curl_init($url);
        $timeout = 4.5;
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    */

    /**
     * @Route("/{_locale}/data.json", name="data")
     */
    public function data(Request $request, LemmaRepository $lemmaRepository): Response
    {
        // route pour exposer le json opentheso

        $lemmas = $lemmaRepository->getBackupFile();
        $response = new Response();
        $response->setContent(json_encode($lemmas));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}
