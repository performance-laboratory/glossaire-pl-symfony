<?php

namespace App\Entity;

use App\Repository\PageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=PageRepository::class)
 * @UniqueEntity("slug")
 */
class Page
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titleFr;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titleEn;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $aboutOrder;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $bodyFr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $bodyEn;

    public function __toString(): string
    {
        return $this->titleFr;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleFr(): ?string
    {
        return $this->titleFr;
    }

    public function setTitleFr(string $titleFr): self
    {
        $this->titleFr = $titleFr;

        return $this;
    }

    public function getTitleEn(): ?string
    {
        return $this->titleEn;
    }

    public function setTitleEn(string $titleEn): self
    {
        $this->titleEn = $titleEn;

        return $this;
    }
    
    public function getAboutOrder(): ?int
    {
        return $this->aboutOrder;
    }

    public function setAboutOrder(?int $aboutOrder): self
    {
        $this->aboutOrder = $aboutOrder;

        return $this;
    }

    public function getBodyFr(): ?string
    {
        return $this->bodyFr;
    }

    public function setBodyFr(?string $bodyFr): self
    {
        $this->bodyFr = $bodyFr;

        return $this;
    }

    public function getBodyEn(): ?string
    {
        return $this->bodyEn;
    }

    public function setBodyEn(?string $bodyEn): self
    {
        $this->bodyEn = $bodyEn;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

}
